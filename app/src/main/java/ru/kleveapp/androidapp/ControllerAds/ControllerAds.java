package ru.kleveapp.androidapp.ControllerAds;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import ru.kleveapp.androidapp.Propeties;


public class ControllerAds {

    ControllerAdsDelegate controllerAdsDelegate;

    public ControllerAds(ControllerAdsDelegate controllerAdsDelegate){
        this.controllerAdsDelegate = controllerAdsDelegate;
    }

    public void checkAd(){
        AndroidNetworking.get(Propeties.ADS_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.getString("adURL").isEmpty()){
                                controllerAdsDelegate.fhjadksfkasjdfkjasdklf(
                                        response.getString("adURL"),
                                        response.getString("adTitle"));
                            }else{
                                controllerAdsDelegate.asdasdasdasd();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            controllerAdsDelegate.asdasdasdasd();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        controllerAdsDelegate.asdasdasdasd();
                    }
                });
    }

}
