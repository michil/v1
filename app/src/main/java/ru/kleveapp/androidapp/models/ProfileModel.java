package ru.kleveapp.androidapp.models;

import ru.kleveapp.androidapp.Propeties;

public class ProfileModel {

    private String name;
    private String web_url;

    public ProfileModel(String name, String web_url) {
        this.name = name;
        this.web_url = web_url;
    }

    public String getName() {
        return name;
    }

    public String getWeb_url() {
        return web_url;
    }

    public boolean isSystem() {
        return web_url.equals(Propeties.NEW_RESOURCE_IDENTIFIER);
    }

}
