package ru.kleveapp.androidapp.screens;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.androidnetworking.AndroidNetworking;

import ru.kleveapp.androidapp.ControllerAds.ControllerAds;
import ru.kleveapp.androidapp.ControllerAds.ControllerAdsDelegate;
import ru.kleveapp.androidapp.R;

public class MainScreen extends AppCompatActivity implements ControllerAdsDelegate {


    Boolean adShowed = false;
    ControllerAds controllerAds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AndroidNetworking.initialize(getApplicationContext());

        controllerAds = new ControllerAds(this);
        controllerAds.checkAd();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (adShowed){
            asdasdasdasd();
        }

    }

    @Override
    public void fhjadksfkasjdfkjasdklf(String url, String title) {
        adShowed = true;
        Intent i = new Intent(this, AdsScreen.class);
        i.putExtra("adURL", url);
        i.putExtra("adTitle", title);
        startActivity(i);
    }

    @Override
    public void asdasdasdasd() {
        Intent i = new Intent(this, ProfilesScreen.class);
        startActivity(i);
        finish();
    }
}
