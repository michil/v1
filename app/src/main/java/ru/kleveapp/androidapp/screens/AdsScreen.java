package ru.kleveapp.androidapp.screens;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

import ru.kleveapp.androidapp.ShowerEngine;
import ru.kleveapp.androidapp.R;

public class AdsScreen extends AppCompatActivity {

    Button closeAdBtn;
    WebView webView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad);

        webView = findViewById(R.id.adWebView);

        closeAdBtn = findViewById(R.id.closeAdBtn);
        closeAdBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeAd();
            }
        });

        String adURL = getIntent().getStringExtra("adURL");
        String adTitle = getIntent().getStringExtra("adTitle");

        setTitle(adTitle);
        loadAd(adURL);
    }


    @SuppressLint("SetJavaScriptEnabled")
    void loadAd(String url){
        closeAdBtn.setVisibility(url.contains("997") ? View.INVISIBLE : View.VISIBLE);

        ShowerEngine showerEngine = new ShowerEngine(this);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(showerEngine);
        webView.loadUrl(url);
    }

    @Override
    public void onBackPressed() {

    }

    void closeAd(){
        finish();
    }
}
