package ru.kleveapp.androidapp.screens;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

import ru.kleveapp.androidapp.ShowerEngine;
import ru.kleveapp.androidapp.R;

public class ProfileScreen extends AppCompatActivity {

    WebView webView;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        webView = findViewById(R.id.profile_webView);

        String loadURL = getIntent().getStringExtra("loadURL");

        assert(loadURL != null);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new ShowerEngine(this));
        webView.loadUrl(loadURL);
    }
}
