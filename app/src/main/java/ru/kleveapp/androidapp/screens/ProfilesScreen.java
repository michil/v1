package ru.kleveapp.androidapp.screens;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import ru.kleveapp.androidapp.ListViewCustomAdapter;
import ru.kleveapp.androidapp.R;
import ru.kleveapp.androidapp.models.ProfileModel;
import ru.kleveapp.androidapp.ProfilesLogic.ProfilesLogic;
import ru.kleveapp.androidapp.ProfilesLogic.ProfilesLogicInterface;

public class ProfilesScreen extends AppCompatActivity implements ProfilesLogicInterface {

    ListView profilesListView;
    ProgressDialog pd;

    ProfilesLogic logic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profiles);

        profilesListView = findViewById(R.id.profilesListView);
        profilesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ProfileModel profileModel = (ProfileModel) parent.getItemAtPosition(position);
                if (profileModel.isSystem()){
                    showRequestNewResource();
                }else {
                    showProfile(profileModel.getWeb_url());
                }
            }
        });


        pd = new ProgressDialog(this);
        pd.setTitle("Загрузка");
        pd.setMessage("Пожалуйста подождите...");
        pd.setCancelable(false);

        logic = new ProfilesLogic(this, this);
    }


    @Override
    public void showloading() {
        pd.show();
    }

    @Override
    public void hideLoading() {
        pd.hide();
    }

    @Override
    public void sdadasdasdas(ArrayList<ProfileModel> profileModels) {
        profilesListView.setAdapter(new ListViewCustomAdapter(this, profileModels));
    }

    @Override
    public void showerror(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showinfo(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    void showProfile(String url){
        Intent i = new Intent(this, ProfileScreen.class);
        i.putExtra("loadURL", url);
        startActivity(i);
    }

    void showRequestNewResource(){
        LayoutInflater factory = LayoutInflater.from(this);

        final View textEntryView = factory.inflate(R.layout.request_new_resource, null);

        final EditText input1 = (EditText) textEntryView.findViewById(R.id.request_new_resource_title);
        final EditText input2 = (EditText) textEntryView.findViewById(R.id.request_new_resource_url);

        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Добавить свой ресурс:")
                .setView(textEntryView)
                .setPositiveButton("Отправить",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int whichButton) {

                        if (input1.getText().toString().isEmpty()){
                            showerror("Поле \"название\" обязательно к заполнению");
                        } else if (input2.getText().toString().isEmpty()){
                            showerror("Поле \"ссылка\" обязательно к заполнению");
                        }else{
                            logic.requestAddNewProfile(input1.getText().toString(), input2.getText().toString());
                        }
                    }
                })
                .setNegativeButton("Отмена",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int whichButton) {

                    }
                });
        alert.show();
    }
}
