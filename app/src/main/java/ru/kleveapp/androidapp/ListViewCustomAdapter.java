package ru.kleveapp.androidapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import ru.kleveapp.androidapp.models.ProfileModel;

public class ListViewCustomAdapter extends BaseAdapter {

    private ArrayList<ProfileModel> profileModels;
    private Context ctx;

    public ListViewCustomAdapter(Context ctx, ArrayList<ProfileModel> profileModels){
        this.profileModels = profileModels;
        this.ctx = ctx;
    }

    @Override
    public int getCount() {
        return profileModels.size();
    }

    @Override
    public Object getItem(int position) {
        return profileModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.profile_cell, parent, false);

        TextView profileTextView = rowView.findViewById(R.id.profile_cell_text_view);

        profileTextView.setText(profileModels.get(position).getName());

        return rowView;

    }
}
