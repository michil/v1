package ru.kleveapp.androidapp.ProfilesLogic;

import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.kleveapp.androidapp.Propeties;
import ru.kleveapp.androidapp.models.ProfileModel;

public class ProfilesLogic {

    Context context;
    ProfilesLogicInterface logicInterface;

    public ProfilesLogic(Context context, ProfilesLogicInterface logicInterface) {
        this.context = context;
        this.logicInterface = logicInterface;

        fetch();
    }

    private void fetch(){
        logicInterface.showloading();

        AndroidNetworking.get(Propeties.PROFILES_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            ArrayList<ProfileModel> profileModels = new ArrayList<ProfileModel>();


                            for (int i = 0; i < response.length(); i++) {

                                ProfileModel profileModel = new ProfileModel(
                                        response.getJSONObject(i).getString("title"),
                                        response.getJSONObject(i).getString("url")
                                );
                                profileModels.add(profileModel);
                            }

                            ProfileModel systemObject = new ProfileModel("Добавить ресурс", Propeties.NEW_RESOURCE_IDENTIFIER);
                            profileModels.add(systemObject);

                            logicInterface.hideLoading();
                            logicInterface.sdadasdasdas(profileModels);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            logicInterface.hideLoading();
                            logicInterface.showerror(e.getLocalizedMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        logicInterface.hideLoading();
                        logicInterface.showerror(anError.getErrorBody());
                    }
                });
    }

    public void requestAddNewProfile(String title, String url){
        AndroidNetworking.get(Propeties.REQUEST_NEW_PROFILE)
                .addQueryParameter("title", title)
                .addQueryParameter("url", url)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            logicInterface.hideLoading();

                            if (response.getBoolean("success")){
                                logicInterface.showinfo(response.getString("message"));
                            }else {
                                logicInterface.showerror(response.getString("message"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            logicInterface.hideLoading();
                            logicInterface.showerror(e.getLocalizedMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        logicInterface.hideLoading();
                        logicInterface.showerror(anError.getErrorBody());
                    }
                });
    }



}
